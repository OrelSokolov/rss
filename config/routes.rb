Rss::Application.routes.draw do
  devise_for :users, :controllers => { 
    :omniauth_callbacks => "users/omniauth_callbacks",
    :registrations => "registrations"
    },
    :path => '',
    :path_names => {
        :sign_up  => 'signup',
        :sign_in  => 'login',
        :sign_out => 'logout'
  }

  resources :users, :only => [:feed, :index, :edit, :destroy]
  resources :subscriptions
  resources :channels

  authenticated do
    root :to => redirect("/feed")
  end

  root :to => 'welcome#index'

  devise_scope :user do
    match "profile"       => "devise/registrations#edit", :as => :edit_user_registration
    match "feed"          => "users#feed",                :as => :feed
    match "favorites"     => "users#favorites",           :as => :favorites
  end

end
