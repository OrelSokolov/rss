class PaginationListLinkRenderer < WillPaginate::ActionView::LinkRenderer

    def to_html
     html = pagination.map do |item|
       item.is_a?(Fixnum) ?
         page_number(item) :
         send(item)
     end.join(@options[:link_separator])

     html
    end

    protected
    def previous_page
        num = @collection.current_page > 1 && @collection.current_page - 1
        previous_or_next_page(num, "<span class='glyphicon glyphicon-chevron-left'></span>", 'previous_page')
      end

      def next_page
        num = @collection.current_page < total_pages && @collection.current_page + 1
        previous_or_next_page(num, "<span class='glyphicon glyphicon-chevron-right'></span>", 'next_page')
      end

      def page_number(page)
      end

      def gap
        ""
      end

    private
     def link(text, target, attributes = {})
      if target.is_a? Fixnum
        attributes[:rel] = rel_value(target)
        target = url(target)
      end
      attributes[:href] = target
      attributes[:class]="btn btn-default btn-lg btn-scrollfeed col-md-2"+ ( ( attributes[:class].include? "next_page") ?  " col-md-push-8 ": "" )
      tag(:div, text, attributes)
     end
 end
