Time::DATE_FORMATS[:today] = "%H:%M"
Time::DATE_FORMATS[:pretty] = lambda { |time| time.strftime("%a, %b %e %l:%M") + time.strftime("%p").downcase }
Time::DATE_FORMATS[:older_than_day] = lambda { |time| time.strftime("%d ") + time.strftime("%b.").downcase + time.strftime(" %Y") }
