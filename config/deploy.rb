set :application, "rss"

set :scm, :git # You can set :scm explicitly or Capistrano will make an intelligent guess based on known version control directory names
set :repository,  "git@bitbucket.org:OrelSokolov/rss.git"
set :scm_passphrase, ""

# set :ssh_options, {:forward_agent => true}
default_run_options[:pty] = true

set :use_sudo, false
set :normalize_asset_timestamps, false

set :user, 'deployer'
# server "192.168.33.11", :app, :web, :db, :primary => true
server "orelsokolov.activeby.net", :app, :web, :db, :primary => true
set :deploy_to, "/home/#{user}/apps/#{application}"
set :deploy_via, :remote_cache

namespace :deploy do
    after 'deploy', 'deploy:migrate'

    %w[start stop restart].each do |command|
      desc "#{command} unicorn server"
      task command, roles: :app, except: {no_release: true} do
        # sudo "killall ruby"
        # sudo "chmod +x /etc/init.d/unicorn_#{application}"
        # sudo "/etc/init.d/unicorn_#{application} #{command}"
        sudo "service unicorn_#{application} #{command}"
        sudo "service nginx #{command}"
        sudo "service updates_listener_#{application} #{command}"
        sudo "service validates_listener_#{application} #{command}"
      end
    end

    task :setup_config do
      sudo "ln -nfs #{current_path}/config/nginx.conf /etc/nginx/sites-enabled/#{application}"

      sudo "cp #{current_path}/config/unicorn_init.sh /etc/init.d/unicorn_#{application}"
      sudo "chmod +x /etc/init.d/unicorn_#{application}"
      run "cd /etc/init.d/ && #{sudo} update-rc.d unicorn_#{application} defaults"

      sudo "cp #{current_path}/script/updates_listener.sh /etc/init.d/updates_listener_#{application}"
      sudo "chmod +x /etc/init.d/updates_listener_#{application}"
      run "cd /etc/init.d/ && #{sudo} update-rc.d updates_listener_#{application} defaults"

      sudo "cp #{current_path}/script/validates_listener.sh /etc/init.d/validates_listener_#{application}"
      sudo "chmod +x /etc/init.d/validates_listener_#{application}"
      run "cd /etc/init.d/ && #{sudo} update-rc.d validates_listener_#{application} defaults"

      run "mkdir -p #{shared_path}/config"
    end
    after "deploy:create_symlink", "deploy:setup_config"


    task :precompile_assets do
        run "cd #{current_path} && bundle exec rake assets:precompile"
    end
    after  'deploy:setup_config', 'deploy:precompile_assets'

    task :update_crontab do
      run "cd #{current_path} && whenever -w"
    end
    after 'deploy:precompile_assets', 'deploy:update_crontab'


    desc "Make sure local git is in sync with remote."
    task :check_revision, roles: :web do
      unless `git rev-parse HEAD` == `git rev-parse origin/master`
        puts "WARNING: HEAD is not the same as origin/master"
        puts "Run `git push` to sync changes."
        exit
      end
    end
    before "deploy", "deploy:check_revision"
end
