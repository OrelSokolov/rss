RAILS_ROOT = File.expand_path(File.dirname(__FILE__)) + '/..'

set :output, {
  :error => "#{RAILS_ROOT}/log/cron-errors.log",
  :standard => "#{RAILS_ROOT}/log/cron-info.log"}

every 5.minutes do
    command "echo 'Run feed:update at #{Time.now}'"
    command "cd /home/deployer/apps/rss/current/ && RAILS_ENV=production rake feed:update"
end

every 1.minutes do
    command "echo 'Cron works... #{Time.now}'"
end
