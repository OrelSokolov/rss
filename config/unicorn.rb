root = "/home/deployer/apps/rss/current"
working_directory root
pid "/home/deployer/unicorn_master.pid"
stderr_path "#{root}/log/unicorn.log"
stdout_path "#{root}/log/unicorn.log"

listen "/tmp/unicorn.rss.sock"
worker_processes 2
timeout 30