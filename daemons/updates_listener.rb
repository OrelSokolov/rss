
puts 'Loading Rails environment...'
require ENV["RAILS_ENV_PATH"]
puts 'Loaded!'

conn = Bunny.new(:automatically_recover => true)
conn.start

updating_channel = conn.create_channel
updating_queue = updating_channel.queue("channel.parse_and_update", :durable => true )

updating_channel.prefetch(1)

puts 'Listen to updates'

begin
  updating_queue.subscribe(:ack => true, :block => true) do |delivery_info, properties, id|
    begin
      channel = Channel.find(id.to_i)
      puts "Updating '"+channel.title+"'' at #{Time.now}"
      fetched_articles = Feedzirra::Feed.fetch_and_parse(channel.url).entries
      already_available_links = Article.all.map(&:link)

      fetched_articles.each do |fetched_article|
        # if Article.where(:link => fetched_article.url) == []
        unless already_available_links.include? fetched_article.url

            image_validate = lambda {
               if fetched_article.image != nil
                 if (open(fetched_article.image).content_type =~ /image/) == 0
                   fetched_article.image
                 end
               end
              }
            if fetched_article.title.length <1_000 &&
               fetched_article.url.length < 1_000
                article = Article.create(
                  :title => fetched_article.title,
                  :link => fetched_article.url,
                  :image => image_validate.call,
                  :summary => Sanitize.clean(fetched_article.summary, :elements => %w|b i|)[0,1_000],
                  :pubdate => fetched_article.published
                )
                channel.articles << article
            end
        end
      end
    rescue ActiveRecord::RecordNotFound
      puts "Hmm...There is no channel with id #{id}"
      puts "May be this channel was deleted before this task started."
    end


    puts 'Channel seems to be updated'
    puts
    updating_channel.ack(delivery_info.delivery_tag)
  end
rescue Interrupt => _
  puts 
  puts 'Interrupted by user'
  conn.close
end
