puts 'Loading Rails environment...'
require ENV["RAILS_ENV_PATH"]
puts 'Loaded!'

conn = Bunny.new(:automatically_recover => true)
conn.start

validating_channel = conn.create_channel
validating_queue = validating_channel.queue("channel.validate")

puts 'Feed tracker started'
validating_queue.subscribe(:block => true) do |delivery_info, properties, url|
  puts "Received a url to validate: #{url}."
  begin
    if Faraday.head(url).status == 200
      if Channel.where(:url => url) == []
        puts 'Adding '+url
        fetched_channel= Feedzirra::Feed.fetch_and_parse(url)
          Channel.create(
            :title => fetched_channel.title,
            :url => url
          )
          Rake::Task["feed:update"].invoke
      else
        puts "Channel already exists."
      end
    else
      puts "Sorry, bad URL."
    end
  rescue Exception => ex
      puts "Something wrong with this URL. Give another"
      puts ex.message
  end
end

