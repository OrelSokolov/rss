require_relative '../../app/helpers/string_helper'


desc "Update feed"
namespace :feed do

   task :export => :environment do
      Article.order(:id).all.each do |country|
        puts "Article.create(#{country.serializable_hash.delete_if {|key, value| ['created_at','updated_at','id'].include?(key)}.to_s.gsub(/[{}]/,'')})"
      end
   end

  task :listen_to_updates => :environment do
    conn = Bunny.new(:automatically_recover => true)
    conn.start

    updating_channel = conn.create_channel
    updating_queue = updating_channel.queue("channel.parse_and_update", :durable => true )

    updating_channel.prefetch(1)

    puts 'Listen to updates'.color(:light_blue)

    begin
      updating_queue.subscribe(:ack => true, :block => true) do |delivery_info, properties, id|
        begin
          channel = Channel.find(id.to_i)
          puts "Updating "+channel.title
          fetched_articles = Feedzirra::Feed.fetch_and_parse(channel.url).entries
          already_available_links = Article.all.map(&:link)
          # puts already_available_links.inspect.color(:light_yellow)

          fetched_articles.each do |fetched_article|
            # if Article.where(:link => fetched_article.url) == []
            unless already_available_links.include? fetched_article.url

                image_validate = lambda {
                   if fetched_article.image != nil
                     if (open(fetched_article.image).content_type =~ /image/) == 0
                       fetched_article.image
                     end
                   end
                  }
                if fetched_article.title.length <1_000 &&
                   fetched_article.url.length < 1_000
                    article = Article.create(
                      :title => fetched_article.title,
                      :link => fetched_article.url,
                      :image => image_validate.call,
                      :summary => Sanitize.clean(fetched_article.summary, :elements => %w|b i|)[0,1_000],
                      :pubdate => fetched_article.published
                    )
                    channel.articles << article
                end
            end
          end
        rescue ActiveRecord::RecordNotFound
          puts "Hmm...There is no channel with id #{id}"
          puts "May be this channel was deleted before this task started."
        end


        puts 'It seems task to be done'.color(:light_green)
        puts 'Continue listening for tasks'.color(:light_blue)
        puts
        updating_channel.ack(delivery_info.delivery_tag)
      end
    rescue Interrupt => _
      puts 
      puts 'Interrupted by user'.color(:light_red)
      conn.close
    end
  end

  task :listen_to_validates => :environment do
    conn = Bunny.new(:automatically_recover => true)
    conn.start

    validating_channel = conn.create_channel
    validating_queue = validating_channel.queue("channel.validate")

      puts 'Feed tracker started'.color(:light_blue)
      validating_queue.subscribe(:block => true) do |delivery_info, properties, url|
        begin
          puts "Received a url to validate: #{url}."
          begin
            if Faraday.head(url).status == 200
              if Channel.where(:url => url) == []
                puts 'Adding '+url.color(:green)
                fetched_channel= Feedzirra::Feed.fetch_and_parse(url)
                  Channel.create(
                    :title => fetched_channel.title,
                    :url => url
                  )
                  Rake::Task["feed:update"].invoke
              else
                puts "Channel already exists.".color(:yellow)
              end
            else
              puts "Sorry, bad URL.".color(:light_red)
            end
          rescue
              puts "Something wrong with this URL. Give another".color(:light_red)
          end
        rescue Interrupt => _
          puts
          puts 'Interrupted by user'.color(:light_red)
        end 
      end

  end

  # task :add_channel, [:url] => :environment do |t, args|
  #   puts args.url
  #   if Faraday.head(args.url).status == 200
  #     if Channel.where(:url => args.url) == []
  #       puts 'Adding '+args.url
  #       fetched_channel= Feedzirra::Feed.fetch_and_parse(args.url)
  #         Channel.create(
  #           :title => fetched_channel.title,
  #           :url => args.url
  #       )
  #     else
  #       puts "Already exists."
  #     end
  #   else
  #     puts "Sorry, bad url."
  #   end
  # end

  # task :fix => :environment do
  #   Article.all.each do |article|
  #     if article.image != nil
  #       #if Faraday.head(article.image).status == 200
  #         #content_type = open(article.image).content_type
  #         #unless (content_type =~ /image/) == 0
  #         #if (article.image =~ /[.]mp4$/) != nil
  #         if (article.image =~ /jpg$/) ==nil
  #           puts article.id
  #           puts article.image
  #           #puts content_type
  #           article.delete
  #           puts "Fixed"
  #         end
  #       #end
  #     end
  #   end
  # end

  task :update => :environment do

    connection = Bunny.new
    connection.start

    updating_channel   = connection.create_channel
    queue = updating_channel.queue("channel.parse_and_update", :durable => true)

    puts "Updating feed.".color(:blue)
    channels = Channel.all
    channels.each do |channel|
      queue.publish("#{channel.id}", :persistent => true)
    end

    puts "Update completed."
    connection.close
  end

end
