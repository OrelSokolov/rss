module StringHelper
end

class String
  def color(color)
    if color == :light_black
      "\033[1;30m#{self}\033[0m"
    elsif color == :black
      "\033[0;30m#{self}\033[0m"
    elsif color == :red
      "\033[0;31m#{self}\033[0m"
    elsif color == :light_red
      "\033[1;31m#{self}\033[0m"
    elsif color == :light_green
      "\033[1;32m#{self}\033[0m"
    elsif color == :green
      "\033[0;32m#{self}\033[0m"
    elsif color == :light_yellow
      "\033[1;33m#{self}\033[0m"
    elsif color == :yellow
      "\033[0;33m#{self}\033[0m"
    elsif color == :light_blue
      "\033[1;34m#{self}\033[0m"
    elsif color == :blue
      "\033[0;34m#{self}\033[0m"
    elsif color == :light_purple
      "\033[1;35m#{self}\033[0m"
    elsif color == :purple
      "\033[0;35m#{self}\033[0m"
    elsif color == :light_cyan
      "\033[1;36m#{self}\033[0m"
    elsif color == :cyan
      "\033[0;36m#{self}\033[0m"
    elsif color == :light_white
      "\033[1;37m#{self}\033[0m"
    elsif color == :white
      "\033[0;37m#{self}\033[0m"
    else
      self
    end
  end
  def indent(value)
    return " "*value+self
  end
end
