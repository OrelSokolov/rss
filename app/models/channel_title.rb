class ChannelTitle < ActiveRecord::Base
  attr_accessible :title, :channel_id

  belongs_to :user
end
