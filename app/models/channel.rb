class Channel < ActiveRecord::Base
  attr_accessible :favorite, :title, :url

  has_many :articles
  has_many :users, :through => :subscriptions, :uniq => true
  #has_and_belongs_to_many :users


  def article(id)
    index = id.to_i-1
    if index < articles.length && index >= 0
      articles[index]
    else
      raise ActiveRecord::RecordNotFound
    end
  end

  def self.validate_and_create(url)
    conn = Bunny.new(:automatically_recover => false)
    conn.start

    ch   = conn.create_channel
    q    = ch.queue("channel.validate")

    ch.default_exchange.publish(url, :routing_key => q.name)
    puts " [x] Sent '#{url}'"

    conn.close 
  end

end
