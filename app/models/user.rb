# require_relative '../helpers/string_helper'

class User < ActiveRecord::Base
  #has_and_belongs_to_many :channels
  has_many :favorites
  has_many :subscriptions
  has_many :articles, through: :favorites, :uniq => true
  has_many :channels, through: :subscriptions, :uniq => true

  ROLES = %w[basic medium premium]
  MAX_SUBSCRIPTIONS = {
    "basic" => 1,
    "medium" => 5,
    "premium" => 20 }

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  # Omniauth
  devise :omniauthable, :omniauth_providers => [:google_oauth2]

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me,
    :login, :firstname, :lastname, :role, :oauth
  attr_accessible :avatar
  has_attached_file :avatar,
    :styles => { :large => "400x400", :medium => "200x200>", :thumb => "100x100>" },
    :default_url => "/assets/:style/missing.png"

  validates :firstname,  presence: true, length: { maximum: 50 }
  validates :lastname,  presence: true, length: { maximum: 50 }
  validates :email, presence: true, :uniqueness => { case_sensitive: false }, length: { maximum: 50 }
  validates :login, presence: true, :uniqueness => true, length: { maximum: 50 }
  validates :role, presence: true, length: { maximum: 50 }

  validates :password, length: { maximum: 50 }
  validates_presence_of :password, :if => :password_required?

  validates_inclusion_of :role, :in => ROLES

  #validates_confirmation_of :password, :if => :password_changed?

  def channel(id)
    index = id.to_i-1
    if index < channels.length && index >= 0
      channels[index]
    else
      raise ActiveRecord::RecordNotFound
    end
  end

  def feed
    articles = []
    channels.each do |channel|
      channel.articles.each do |article|
        articles << article
      end
    end
    articles
  end


  def self.find_for_google_oauth2(access_token, signed_in_resource=nil)
    data = access_token.info
    user = User.where(:email => data["email"]).first

    # puts ('#'*80).color(:yellow)
    # puts data.inspect
    # puts ('#'*80).color(:yellow)

    unless user

        # puts ('#'*100).color(:light_blue)
        # puts 'CREATING'
        # puts ('#'*100).color(:light_blue)

        user = User.create!(
            firstname: data["first_name"],
            lastname: data["last_name"],
            login: data["name"],
	    		  email: data["email"],
            role: "basic",
	    		  password: Devise.friendly_token[0,20]
	    		  )
    end
    user
  end
end
