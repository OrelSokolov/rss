class Article < ActiveRecord::Base
  attr_accessible :link, :pubdate, :summary, :title, :image
  attr_accessible :channel, :channel_id

  has_many :users, :through => :favorites, :uniq => true
  belongs_to :channel

  def self.search(ids, request)
    if request
      Article
          .where(id: ids)
          .where('title @@ :q or summary @@ :q', q: request)
          .order('pubdate desc')
          # .where(id: ids)
          # .where('title LIKE LOWER(?) OR summary LIKE LOWER(?)',
          #        "%#{request}%",
          #        "%#{request}%")
          # .order('pubdate DESC')
    else
      Article.where(id: ids).order('pubdate DESC')
    end
  end

  def site_name
    url = Domainatrix.parse(link)
    "#{(url.subdomain ? nil : url.subdomain+'.' )}#{url.domain}.#{url.public_suffix}"
  end

  def date
    pubdate
  end
end
