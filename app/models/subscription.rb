class Subscription < ActiveRecord::Base
  attr_accessible :title, :user_id, :channel_id
  validates :title,  presence: true, length: { maximum: 100 }

  belongs_to :channel
  belongs_to :user
end
