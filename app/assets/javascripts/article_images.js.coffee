
$(document).ready(->

    imgSelector = "div.article > img"

    resizeImage = (img) ->

      IMG = $(img)
      CONTENT = IMG.parent().find("div.content")
      IMG_H = ->
        parseInt(IMG.css("height").slice(0, -2))
      CONTENT_H = ->
        parseInt(CONTENT.css("height").slice(0, -2))
      DIFF = ->
        Math.abs(IMG_H()-CONTENT_H())
      MAX_HEIGHT = IMG.parent().width()*0.3
      EPS = 0


      for x in [1..50]
        if DIFF() > EPS && IMG_H() < MAX_HEIGHT
          if IMG_H() > CONTENT_H()
            IMG.css("height", IMG_H()-DIFF()/2)
          else
            IMG.css("height", IMG_H()+DIFF()/2)
        else
          break
      if IMG_H() < 60
        # IMG.parent().css('width', "60%")
        # IMG.parent().css('margin-left', "20%")
        # IMG.parent().css('margin-right', "20%")
        #CONTENT.css("display", "none")
        IMG.parent().find("h1").css("text-align", "center")
        IMG.css("display", "block")
        IMG.css("float", "none")
        IMG.css("height", "100%")
        IMG.css("width", "100%")
        IMG.css("margin-bottom", "1em")

    resizeAll = () ->
      $(imgSelector).each((i, img)->
        resizeImage img
      )

    $(imgSelector).each((i, img)->
      $(img).load(->
        resizeImage(img)
      )
    )


    window.onresize = resizeAll
)
