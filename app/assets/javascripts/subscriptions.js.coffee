$(document).ready(->
  $(".edit-channel-title").on("click", (e)->
    e.stopPropagation()
    $(this)
      .parent()
      .find(".edit-channels-title-form")
      .toggle()
    $(this)
      .parent()
      .find("input[type=submit]")
      .toggle()
  )

)
