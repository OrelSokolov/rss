$(document).ready(->
  offset = $("div.body > div.body-content").css("margin-top").slice(0,-2)

  $("nav").on("mouseenter", ->
    $(this).removeClass("nav_hidden")
  )

  $("nav").on("mouseleave", ->
    if (window.pageYOffset > offset)
      $(this).addClass("nav_hidden")
  )

  $(window).scroll(->
    if window.pageYOffset > offset
      $("nav").addClass('nav_hidden')
    else
      $("nav").removeClass('nav_hidden')
  )
)
