$(document).ready(->

  show_settings = () ->
    $(".user-settings").css("display", "block")
    $(".danger-zone").css("display", "block")
    $(".profile").css("display", "none")

  hide_settings = () ->
    $(".user-settings").css("display", "none")
    $(".danger-zone").css("display", "none")
    $(".profile").css("display", "block")

  $("#edit-user-settings").on("click", ->
    show_settings()
  )

  $("#hide-settings").on("click", ->
    hide_settings()
  )

)
