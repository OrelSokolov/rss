class ArticlesController < ApplicationController
  before_filter :authenticate_user!

  def index
    @articles = current_user.channel(params[:channel_id]).articles.paginate(:page => params[:page], :per_page => 3)
  end

  def show
    @article = current_user.channel(params[:channel_id]).article(params[:id])
  end
end
