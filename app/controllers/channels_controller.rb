class ChannelsController < ApplicationController
  before_filter :authenticate_user!


  def create
    Channel.validate_and_create params[:channel_url]
    flash[:notice] = t('channels.alerts.i_will_check_this')
    redirect_to subscriptions_path
  end

end
