# coding: utf-8
class SubscriptionsController < ApplicationController
  before_filter :authenticate_user!

  def index
    @subscriptions = current_user.subscriptions
    if @subscriptions == []
      flash[:notice] = t("subscriptions.alerts.subscribe_any_channel")
    end
    sub_ids = @subscriptions.map(&:channel_id)
    @channels = []
    all_channels = Channel.all
    all_channels.each do |channel|
      @channels << channel unless sub_ids.include? channel.id
    end
  end

  def create
    current_subscriptions_amount = current_user.subscriptions.length
    if current_subscriptions_amount < User::MAX_SUBSCRIPTIONS[current_user.role]
      if params[:channel_id]
        channel_id = params[:channel_id]
        channel = Channel.find(channel_id)
        Subscription.create(:title => channel.title,
                         :user_id => current_user.id,
                         :channel_id => channel_id )
      end
    else
      flash[:notice] = t('subscriptions.alerts.account_limitations')
    end
    redirect_to subscriptions_path
  end

  def edit
    if params[:id] && params[:new_title]
      subscription = Subscription.find(params[:id])
      subscription.title = params[:new_title]
      subscription.save
    end
    redirect_to subscriptions_path
  end

  def destroy
    if params[:id]
      Subscription.find(params[:id]).destroy
    end
    redirect_to subscriptions_path
  end

end
