# encoding: utf-8
class UsersController < ApplicationController
  before_filter :authenticate_user!


  def edit
    # puts ("#"*80).color(:yellow)
    # puts params.inspect.color(:yellow)
    # puts ("#"*80).color(:yellow)
  end

  def feed
    @page = params[:page]
    if params[:favorite_id]!=nil
      current_user.articles << Article.find(params[:favorite_id])
      redirect_to feed_path(:page => params[:page])
    else
      articles = []
      current_user.channels.each do |channel|
        channel.articles.each do |article|
          articles << article
        end
      end
      ids = articles.map(&:id)

      @feed = Article.search(ids, params[:search]).page(params[:page])

      if @feed == []
        if current_user.channels == []
          flash[:notice] = I18n.t("subscriptions.alerts.subscribe_any_channel")
          redirect_to subscriptions_path
        else
          flash.now[:notice] = t('users.no_feed')
          render '/articles/_not_found'
        end
      else
        render 'feed', :locals => { :not_favorite => true }
      end
    end


  end

  def favorites
    @page = params[:page]
    @feed = current_user.articles.page(params[:page])
    if params[:favorite_id]!=nil
      current_user.favorites.where(:article_id => params[:favorite_id]).destroy_all
      redirect_to favorites_path(:page => params[:page])
    else
      if @feed == []
        flash[:notice] = t('users.no_favs')
        redirect_to feed_path(:page => params[:page])
      else
        render 'feed', :locals => { :not_favorite => false }
      end
    end
  end

end
