require 'spec_helper'

describe "profile page body" do
  before :each do
    login_as_new(:user)
    visit edit_user_registration_path
    expect(current_path).to eq(edit_user_registration_path)
  end

  subject { page }

  # Sign up form

  it "visible content" do
    within('div.profile') do
      find('h1').should be_visible
      find('h1 > span').should be_visible
      find('img').should be_visible
      find('div.personal-data').should be_visible
    end
  end 

  it "invisible content" do
    within('div.user-settings', :visible => false) do
      should have_css('h1', :visible => false)
      should have_css('input#user_firstname', :visible => false)
      should have_css('input#user_lastname', :visible => false)
      should have_css('input#user_email', :visible => false)
      should have_css('input#user_password', :visible => false)
      should have_css('input#user_password_confirmation', :visible => false)
      should have_css('input#user_current_password', :visible => false)
      should have_css("input[type=submit]", :visible => false)
    end 
  end 

  after :each do
    Capybara.reset_sessions!
    User.last.destroy
  end
end

describe "delete_user_button" do
  before :each do
    @user = login_as_new(:user)
    visit edit_user_registration_path
    expect(current_path).to eq(edit_user_registration_path)
    User.where(:email => @user.email).to_a.should_not eq([])
  end

  it "delete user" do
    within('div.danger-zone', :visible => false) do
      find('input[type=submit]', :visible => false).click
    end
    User.where(:email => @user.email).to_a.should eq([])
  end

end

