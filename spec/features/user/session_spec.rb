describe "session" do
  before(:each) { login_as_new(:user) }
  after(:each) { User.last.destroy }

  it "logout" do
    find("a.logout").click
    current_path.should eq(new_user_session_path)
  end

end
