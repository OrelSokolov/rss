
describe "subscriptions" do
	before :each do
		login_as_new(:user)
		@channel = FactoryGirl.create(:channel)
	end

	subject { page }

	it "add" do
		visit subscriptions_path
		should_not have_css("ul.subscriptions  input[value=#{@channel.title}]")
		within(:css, 'ul.channels') do
			find("input[value=#{@channel.title}]").click
		end
		visit subscriptions_path
		should have_css("ul.subscriptions  input[value=#{@channel.title}]")
	end

	it "remove" do
		visit subscriptions_path
		within(:css, 'ul.channels') do
			find("input[value=#{@channel.title}]").click
		end
		should have_css("ul.subscriptions  input[value=#{@channel.title}]")
		within(:css, 'ul.subscriptions') do
			find("input[value=#{@channel.title}]").click
		end
		visit subscriptions_path
		should_not have_css("ul.subscriptions  input[value=#{@channel.title}]")
	end

	after :each do
		Channel.last.destroy
		User.last.destroy
	end
end