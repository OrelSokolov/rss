require 'spec_helper'

describe "sign_up page body" do
  before :each do
    visit new_user_registration_path
  end

  subject { page }

  # Sign up form

  it { should have_selector('h2', :text => "#{I18n.t('devise.registrations.new.title')}") }

  it { should have_field(:user_firstname) }

  it { should have_field(:user_lastname) }

  it { should have_field(:user_login) }

  it { should have_field(:user_email) }

  it { should have_field(:user_password) }

  it { should have_field(:user_password_confirmation) }

  it { should have_button(I18n.t('devise.registrations.new.signup')) }

end


describe "resulf of user's registration" do
  before :all do
    @user = FactoryGirl.build(:user)
    visit new_user_registration_path
    within(:css, "div.register-well") do
      fill_in "user_firstname", with: @user.firstname
      fill_in "user_lastname",  with: @user.lastname
      fill_in "user_login",     with: @user.login
      fill_in "user_email",     with: @user.email
      fill_in "user_password",  with: @user.password
      fill_in "user_password_confirmation", with: @user.password
    end 
    find('input[type=submit]').click
    expect(current_path).to eq(subscriptions_path)
  end 

  subject { @user }

  it "checks if user has appropriate values" do
    User.last.firstname.should eq(@user.firstname)
    User.last.lastname.should  eq(@user.lastname)
    User.last.login.should     eq(@user.login)
    User.last.email.should     eq(@user.email)
    User.last.role.should      eq("basic")
  end

  after :all do
    User.last.destroy
  end 
end
