require 'spec_helper'

describe "sign_in page" do

  before :each do
    visit new_user_session_path
  end

  subject { page }

  # Sign up form

  it { should have_selector('h1', :text => "#{I18n.t('devise.sessions.new.title')}") }

  it { should have_field(:user_email) }

  it { should have_field(:user_password) }

  it { should have_field(:user_password) }

  it { should have_button(I18n.t('devise.sessions.new.signin')) }

  it { should have_link( I18n.t("devise.shared.links.forgot_password"),  href: new_user_password_path) }

end

