
describe "favorites page" do
	before :each do
		@channel = FactoryGirl.create(:channel)
		@article = FactoryGirl.create(:article)
		@article2 = FactoryGirl.create(:article)
		@channel.articles << @article
		@channel.articles << @article2
		login_as_new(:user)
		visit subscriptions_path
		find("input[value=#{@channel.title}]").click
	end

	subject { page }

	it "try to visit favorites without favorites" do
		visit favorites_path
		current_path.should eq(feed_path)
	end

	it "adding ro favorites" do
		visit feed_path
		within(:css, "div.article[@data-link=#{@article.link}]") do
			find('a.to-favs').click
		end
		visit favorites_path
		should have_css("div.article[@data-link=#{@article.link}]")
	end

	it "try to visit favorites with favorites" do
		visit favorites_path
		current_path.should eq(feed_path)
	end

	it "remove from favorites" do
		visit favorites_path
		within(:css, "div.article[@data-link=#{@article.link}]") do
			find('a.to-favs').click
		end
		visit favorites_path
		should have_css("div.article[@data-link=#{@article.link}]")
	end

	after :each do
		Article.all.each { |i| i.destroy }
		Channel.all.each { |i| i.destroy }
		User.last.destroy
	end
end