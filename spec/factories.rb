FactoryGirl.define do
  factory :user do
    firstname "Oleg"
    lastname  "Orlov"
    sequence(:login)   { |i|   "Login#{i}" }
    sequence(:email)   { |i|  "email#{i}@example.com" }
    password "123456789"
    role "basic"
    password_confirmation "123456789"
  end

  factory :article do
  	title 				"Some title"
  	sequence(:link) 	{ |i| "link#{i}" }
  	sequence(:summary)	{ |i| "summary#{i}" }
  	sequence(:pubdate) 	{ |i| Date.today }
  	image				"image_link"
  end

  factory :channel do
  	sequence(:title) {|i| "title#{i}"}
  	sequence(:url)  {|i| "link#{i}"}
  end
end
