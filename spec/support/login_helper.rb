def login_as_new(sym)
    visit new_user_session_path
    user = FactoryGirl.create(sym)
    fill_in :user_email, :with => user.email
    fill_in :user_password, :with => user.password
    find('input[type=submit]').click
    user
end

