source 'https://rubygems.org'
ruby "1.9.3"

gem 'rails', '3.2.14'

# Bundle edge Rails instead:
# gem 'rails', :git => 'git://github.com/rails/rails.git'

gem "pg", "~> 0.16.0"

gem 'jquery-rails'
gem 'jquery-rails'
# gem "amqp", "~> 1.0.2"
gem "bunny", "~> 1.0.3"
gem "haml", "~> 4.0.3"
gem "devise", "~> 3.1.0"
gem "devise-i18n", "~> 0.9.1"
gem "omniauth-google-oauth2", "~> 0.2.1"
gem "cancan", "~> 1.6.10"
gem "i18n", "~> 0.6.5"
gem 'rails-i18n', '~> 3.0.0'
gem "gravatar_image_tag", "~> 1.1.3"
gem "feedzirra", :github => "pauldix/feedzirra" # atom parser
gem "pismo", "~> 0.7.4" # images from sites
gem "domainatrix", "~> 0.0.11" # pretty urls
gem "rmagick", "~> 2.13.2"    # image processing
gem "paperclip", "~> 3.5.1"   # image processing
gem "whenever", "~> 0.8.4", :require => false    # cron binding
gem "will_paginate", "~> 3.0.5"
gem "font-awesome-sass", "~> 4.0.1"
gem "daemons", "~> 1.1.9"

# Gems used only for assets and not required
# in production environments by default.
group :assets do
  gem "bootstrap-sass-rails", "~> 3.0.0.3"
  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.1'

  # See https://github.com/sstephenson/execjs#readme for more supported runtimes
  # gem 'therubyracer', :platforms => :ruby

  gem 'uglifier', '>= 1.0.3'
end

gem "capybara", "~> 2.1.0"

group :production do
end

group :development do
  gem "launchy", "~> 2.3.0"
  gem "rspec-rails", "~> 2.14.0"
  #gem "guard", "~> 1.8.3"
  gem "guard", "~> 2.0.5"
  gem "guard-bundler", :github => 'guard/guard-bundler', :branch => "v2.0"
  gem "guard-shell", "~> 0.5.1"
  gem "guard-rspec", "~> 3.1.0"
  gem "guard-spork", "~> 1.5.1"
  gem "guard-livereload", "~> 2.0.0", require: false
  gem "rack-livereload", "~> 0.3.15"
  gem "spork", "~> 0.9.2"
  gem "libnotify", "~> 0.8.2"
  gem "progress_bar", :github => "paul/progress_bar", :require => false  # nice for rake tasks
  gem "faraday", "~> 0.8.8" # nice http requests
  gem "sanitize", "~> 2.0.6"

  gem "webrick", "~> 1.3.1" # To avoid stupid warnings.

end

group :test do
  gem "selenium-webdriver", "~> 2.37.0"
  gem "launchy", "~> 2.3.0"
  gem "rspec-rails", "~> 2.14.0"
  gem "spork", "~> 0.9.2"
  gem "timecop", "~> 0.6.3"
  gem "email_spec", "~> 1.5.0"
  gem "factory_girl_rails", "~> 4.2.1"
  gem "database_cleaner", "~> 1.1.1"
end

# To use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# To use Jbuilder templates for JSON
# gem 'jbuilder'

# Use unicorn as the app server
gem 'unicorn'

# Deploy with Capistrano
gem 'capistrano', '~> 2.15.5'

# To use debugger
# gem 'debugger'

gem 'therubyracer'
