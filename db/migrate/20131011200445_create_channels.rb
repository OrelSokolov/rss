class CreateChannels < ActiveRecord::Migration
  def change
    create_table :channels do |t|
      t.text :title
      t.boolean :favorite
      t.text :url

      t.timestamps
    end
  end
end
