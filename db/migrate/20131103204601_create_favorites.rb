class CreateFavorites < ActiveRecord::Migration
  def change
    create_table :favorites do |t|
      t.belongs_to :user
      t.belongs_to :article
      t.datetime :favorites_date
      t.timestamps
    end
  end
end
