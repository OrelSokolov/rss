class AddFirstnameAndLastnameForUsers < ActiveRecord::Migration
  def up
     add_column :users, :firstname, :string
     add_column :users, :lastname, :string
  end

  def down
     remove_column :users, :firstname, :string
     remove_column :users, :lastname, :string
  end
end
