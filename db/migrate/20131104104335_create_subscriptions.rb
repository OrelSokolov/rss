class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.string :title
      t.belongs_to :user
      t.belongs_to :channel

      t.timestamps
    end
  end
end
