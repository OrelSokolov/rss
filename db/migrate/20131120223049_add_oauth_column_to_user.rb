class AddOauthColumnToUser < ActiveRecord::Migration
  def change
    add_column :users, :oauth, :boolean
  end
end
