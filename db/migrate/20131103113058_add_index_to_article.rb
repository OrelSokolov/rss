class AddIndexToArticle < ActiveRecord::Migration
  def change
    add_index :articles, :title
    add_index :articles, :summary
  end
end
