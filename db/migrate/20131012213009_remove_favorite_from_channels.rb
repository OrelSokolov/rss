class RemoveFavoriteFromChannels < ActiveRecord::Migration
  def up
    remove_column :channels, :favorite
  end

  def down
    add_column :channels, :favorite, :boolean
  end
end
