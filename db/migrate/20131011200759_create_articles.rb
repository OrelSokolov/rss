class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.text :title
      t.text :link
      t.text :summary
      t.datetime :pubdate
      t.boolean :favorite

      t.timestamps
    end
  end
end
