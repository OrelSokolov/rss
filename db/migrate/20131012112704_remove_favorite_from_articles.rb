class RemoveFavoriteFromArticles < ActiveRecord::Migration
  def up
    remove_column :articles, :favorite
  end

  def down
    add_column :articles, :favorite, :boolean
  end
end
