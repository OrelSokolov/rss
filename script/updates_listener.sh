#!/bin/sh
### BEGIN INIT INFO
# Provides:          updates_listener_rss
# Required-Start:    $remote_fs $syslog rabbitmq-server
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Manage unicorn server
# Description:       Start, stop, restart unicorn server for a specific application.
### END INIT INFO
set -e

# Feel free to change any of the following variables for your app:
TIMEOUT=${TIMEOUT-60}
APP_ROOT=/home/deployer/apps/rss/current
# PID=/tmp/updates_listener_rss.pid
CMD="cd $APP_ROOT; RAILS_ENV=production ruby script/updates_listener.rb"
AS_USER=deployer
set -u

run () {
  if [ "$(id -un)" = "$AS_USER" ]; then
    eval $1
  else
    su -c "$1" - $AS_USER
  fi
}

case "$1" in
start)
  run "$CMD start"
  ;;
stop)
  run "$CMD stop"
  ;;
restart)
  run "$CMD restart"
  ;;
*)
  echo >&2 "Usage: $0 <start|stop|restart>"
  exit 1
  ;;
esac