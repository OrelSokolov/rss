require 'rubygems'
require 'daemons'

DAEMON="updates_listener.rb"

ENV["APP_ROOT"] ||= File.expand_path("#{File.dirname(__FILE__)}/..")
ENV["RAILS_ENV_PATH"] ||= "#{ENV["APP_ROOT"]}/config/environment.rb"

path_to_script = "#{ENV["APP_ROOT"]}/daemons/#{DAEMON}"

Daemons.run(path_to_script, dir_mode: :normal, log_dir: "#{ENV['APP_ROOT']}/log/", log_output: true, dir: "#{ENV["APP_ROOT"]}/tmp/pids") 